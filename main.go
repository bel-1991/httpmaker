package main

import (
	"net/http"
	"log"
	"net/url"
	"io"
	"bytes"
	"mime/multipart"
	"os"
	"io/ioutil"
)

func main()  {
	if os.Args[1] == "get" {
		log.Println("make get request")
		makeHttpGET()
		return
	}
	if os.Args[1] == "post" {
		log.Println("make post request")
		makeHttpPOST()
		return
	}
}

func makeHttpGET() {
	params := url.Values{"dd": {"888sdsds"}, "aa": {"bbb"}}

	url := &url.URL{
		Scheme:     "http",
		Host:       "localhost:8080",
		Path:       "/",
		RawQuery:   params.Encode(),
	}

	client := &http.Client{}

	rs, err := client.Get(url.String())
	if err != nil {
		log.Fatal("error request ", err.Error())
	} else {
		bodyBuffer, _ := ioutil.ReadAll(rs.Body)
		log.Print(string(bodyBuffer))
		log.Println("request succesed")
	}
}

func makeHttpPOST() {
	params := url.Values{"dd": {"888sdsds"}, "aa": {"bbb"}}

	url := &url.URL{
		Scheme:     "http",
		Host:       "localhost:8080",
		Path:       "/",
		RawQuery:   params.Encode(),
	}
	Upload(url, "filename.txt")
}

func Upload(url *url.URL, file string) (err error) {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	f, err := os.Open(file)
	if err != nil {
		log.Println("can not open file ", file)
		return
	}
	defer f.Close()
	fw, err := w.CreateFormFile("template", file)
	if err != nil {
		log.Println("can not create FormValue")
		return
	}
	if _, err = io.Copy(fw, f); err != nil {
		log.Println("can not write")
		return
	}

	w.Close()

	req, err := http.NewRequest("POST", url.String(), &b)
	if err != nil {
		log.Println("error create POST request")

		return
	}
	req.Header.Set("Content-Type", w.FormDataContentType())


	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("error  POST request")
		return
	}

	// Check the response
	if res.StatusCode != http.StatusOK {
		log.Fatalf("bad status: %s", res.Status)
	}

	bodyBuffer, _ := ioutil.ReadAll(res.Body)
	log.Print(string(bodyBuffer))
	return
}

